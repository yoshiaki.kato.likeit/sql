CREATE DATABASE sql_question DEFAULT CHARACTER SET utf8;

USE sql_question;

-- 問１
CREATE TABLE item_category (
	category_id INT NOT NULL AUTO_INCREMENT primary key,
	category_name VARCHAR(256) NOT NULL
);

-- 問２
CREATE TABLE item (
	item_id INT NOT NULL AUTO_INCREMENT primary key,
	item_name VARCHAR(256)NOT NULL,
	item_price INT NOT NULL,
	category_id INT
);

-- 問３
INSERT INTO item_category(category_id,category_name) VALUES (1,'家具');
INSERT INTO item_category(category_id,category_name) VALUES (2,'食品');
INSERT INTO item_category(category_id,category_name) VALUES (3,'本');

-- 問４
INSERT INTO item(item_id,item_name,item_price,category_id) VALUES (1,'堅牢な机',3000,1);
INSERT INTO item(item_id,item_name,item_price,category_id) VALUES (2,'生焼け肉',50,2);
INSERT INTO item(item_id,item_name,item_price,category_id) VALUES (3,'すっきりわかるJava入門',3000,3);
INSERT INTO item(item_id,item_name,item_price,category_id) VALUES (4,'おしゃれな椅子',2000,1);
INSERT INTO item(item_id,item_name,item_price,category_id) VALUES (5,'こんがり肉',500,2);
INSERT INTO item(item_id,item_name,item_price,category_id) VALUES (6,'書き方ドリルSQL',2500,3);
INSERT INTO item(item_id,item_name,item_price,category_id) VALUES (7,'ふわふわのベッド',30000,1);
INSERT INTO item(item_id,item_name,item_price,category_id) VALUES (8,'ミラノ風ドリア',300,2);

-- 問５
SELECT item_name,item_price FROM item WHERE category_id = 1;

-- 問６
SELECT item_name,item_price FROM item WHERE item_price >= 1000;

-- 問７
SELECT item_name,item_price FROM item WHERE item_name LIKE '%肉%';

-- 問８
SELECT
	item_id,
	item_name,
	item_price,
	category_name
FROM item
INNER JOIN item_category ON item.category_id = item_category.category_id;

-- 問９
SELECT
	category_name,
	
	SUM(item_price) AS total_price
	
FROM item
INNER JOIN item_category ON item.category_id = item_category.category_id
GROUP BY item.category_id
ORDER BY total_price DESC

;
